import React, { Component } from 'react';
import Burger from './components/Burger/Burger';
import Ingredients from './components/Ingredients/Ingredients';
import './App.css';

class App extends Component {
    state = {
        ingredients: [
            {id: 0, name: 'Burger with sauce', count: 20},
            {id: 1, name: 'Meat', count: 0},
            {id: 2, name: 'Cheese', count: 0},
            {id: 3, name: 'Beacon', count: 0},
            {id: 4, name: 'Salad', count: 0}
        ]
    };

    CheckIngredients = (id) => {
        const index = this.state.ingredients.findIndex(p => p.id === id);
        const ingredient = {...this.state.ingredients[index]};
        ingredient.count++;

        const ingredients = [...this.state.ingredients];
        ingredients[index] = ingredient;

        this.setState({ingredients: ingredients});
    };


  render() {
      let Ingredient = [...this.state.ingredients];
      for (let i = 1; i < 5; i++) {
          const Ingred = Ingredient[i].count;
    return (
        <div className="container">
            <div className="ingredients">
                <p className="name">Ingredients:</p>
                <Ingredients>
                    <button  className="add" onClick={this.CheckIngredients.bind(this, 1)}>Add:</button>
                    <p className="quantity">Кол-во: {Ingred}</p>
                    <button className="trash"><i className="far fa-trash-alt"></i></button>
                </Ingredients>
            </div>
            <div className="burger">
                <p className="name">Burger:</p>
                <Burger>
                    <p>Цена: {this.state.ingredients[0].count}</p>
                </Burger>
            </div>
        </div>
    )}
  }
}

export default App;
