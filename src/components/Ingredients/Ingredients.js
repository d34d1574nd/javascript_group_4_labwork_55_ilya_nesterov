import React, {Component, Fragment} from 'react';
import './Ingredients.css'

import meatImage from '../../assets/meat.png';
import cheeseImage from '../../assets/cheese.png';
import beaconImage from '../../assets/beacon.png';
import saladImage from '../../assets/salad.png';

class Ingredient extends Component {
    ingredient = [
        {name: 'Meat', price: 50, image: meatImage},
        {name: 'Cheese', price: 20, image: cheeseImage},
        {name: 'Beacon', price: 30, image: beaconImage},
        {name: 'Salad', price: 5, image: saladImage}
    ];

    render() {
        return (
            <Fragment>
                {this.ingredient.map((comp) => {
                        return (
                            <div className="ingredient">
                                <img className="image" src={comp.image} alt={comp.image}/>
                                <p className="nameIngredient">{comp.name}</p>
                                {this.props.children}
                            </div>
                        )
                    }
                )}
            </Fragment>
        )
    }
}

export default Ingredient;