import React, {Component} from 'react';
import './Burger.css';

class Burger extends Component {
    render() {
        return (
            <div className="Burger">
                <div className="BreadTop">
                    <div className="Seeds1"></div>
                    <div className="Seeds2"></div>
                </div>
                <div className="BreadBottom"></div>
                {this.props.children}
            </div>
        );
    }
};

export default Burger;





